Installer le projet web sur un ordinateur quelconque :

1) Installer Xampp en acc�dant au lien suivant: 
https://www.apachefriends.org/fr/download.html

2) Lancer Xampp

3) D�marrer le serveur Apache et MySQL

4) Ouvrir un navigateur web quelconque

5) Copier l'url suivante dans la barre de recherche :
localhost/

6) Cliquer sur phpMyAdmin

7) Ajouter une base de donn�es en prenant soin de la nommer "mywishlist"
(ne pas prendre en compte les guillemets)

8) Ajouter les  tables  et les donn�es en copiant le script SQL fourni
en annexe

9) Ouvrir le dossier nomm� "Xampp" sur votre ordinateur

10) Ouvrir le dossier nomm� "HTDOCS"

11) Copier ici le dossier "projetWeb" fourni en annexe

12) Copier l'url suivante dans la barre de recherche :
localhost/projetWeb

Bonne navigation !