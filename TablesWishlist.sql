create table compte(
id int(255),
email varchar(100),
login varchar(100),
mdp varchar(999),
PRIMARY KEY (id)
);

create table participant(
id int(11),
nom varchar(15),
prenom varchar(15),
PRIMARY KEY (id)
);

create table liste(
no int(11),
user_id int(11),
titre varchar(255),
description text,
expiration date,
token varchar(255),
message varchar(1200),
publique tinyint(1),
PRIMARY KEY (no),
FOREIGN KEY (user_id) REFERENCES compte(id),
);

create table item(
id int(11),
liste_id int(11),
nom text,
descr text,
img text,
url text,
tarif decimal(5,2),
statut varchar(15),
participant_id int(11),
PRIMARY KEY (id),
FOREIGN KEY (liste_id) REFERENCES liste(no),
FOREIGN KEY (participant_id) REFERENCES participant(id)
);

create table message(
id int(11),
type varchar(15),
text varchar(1200),
liste_id int(11),
item_id int(11),
PRIMARY KEY (id),
FOREIGN KEY (liste_id) REFERENCES liste(no),
FOREIGN KEY (item_id) REFERENCES item(id)
);